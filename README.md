# pbfclipper

**Pbfclipper** is a Python library and utility to obtain an OpenStreetmap Protobuf file clipped to a custom extent. It downloads and caches the smallest extract from [Geofabrik](https://download.geofabrik.de/) or [bbbike](https://download.bbbike.org/osm/bbbike/) that covers the requested area, then clips the data to an extent specified as a polygon.

## Installation

**Pdfclipper** is available from PyPi:

```
pip install pbfclipper
```

Alternatively, it can be built from source:

```
git clone https://gitlab.com/christoph.fink/pbfclipper
cd pbfclipper
pip install .
```

## Usage

### Command line tool

```
pbfclipper --polygon "POLYGON((24.40 60.12, 24.45 60.11, 24.46 60.13, 24.40 60.12))" --output "kirkkonummi.osm.pbf"
```

### Library

```
from pbfclipper import PbfClipper
import shapely.geometry
import shapely.wkt

polygon = shapely.wkt.loads(
    "POLYGON((24.40 60.12, 24.45 60.11, 24.46 60.13, 24.40 60.12))"
)
PbfClipper().clip(polygon, "/tmp/kirkkonummi.osm.pbf")

polygon = shapely.geometry.box(-92.65, -1.81, -88.65, 2.2)
PbfClipper().clip(polygon, "/tmp/galapagos.osm.pbf")
```
