# cython: language_level=3

"""Clip a PBF file to a polygon’s extent."""


import typing

import shapely.geometry
import osmium


class PbfFileClipper(osmium.SimpleHandler):
    """Clip a PBF file to a polygon’s extent."""

    def clip(
            self,
            input_filename: str,
            clip_polygon: shapely.geometry.Polygon,
            output_filename: str
    ) -> None:
        """
        Clip a .osm.pbf file to the extent of clip_polyon.

        Arguments:
            input_filename:     read this .pbf file
            clip_polygon:       copy data that are inside this polygon
            output_filename:    write the new file to this location (file extension
                                determines file format, see https://docs.osmcode.org/
                                pyosmium/latest/ref_osmium.html#simplewriter)
        """
        self.clip_polygon = clip_polygon
        self.extent = osmium.osm.Box(
            osmium.osm.Location(*clip_polygon.bounds[0:2]),
            osmium.osm.Location(*clip_polygon.bounds[2:4])
        )
        self.input_filename = input_filename
        self.writer = osmium.SimpleWriter(output_filename)

        self.apply_file(
            self.input_filename,
            locations=True,
            idx="flex_mem"
        )

    def _clip_polygon_contains(self, location: osmium.osm.Location) -> bool:
        """
        Check whether location is inside self.clip_polygon.

        Sped-up (hopefully) because of short-circuit evaluation.
        """
        return (
                self.extent.contains(location)
                and self.clip_polygon.contains(
                    shapely.geometry.Point(
                        location.lon,
                        location.lat
                    )
                )
        )

    def node(self, node: osmium.osm.Node) -> None:
        """Process a node."""
        if self._clip_polygon_contains(node.location):
            self.writer.add_node(node)

    def way(self, way: osmium.osm.Way) -> None:
        """Process a way."""
        for node in way.nodes:
            if self._clip_polygon_contains(node.location):
                self.writer.add_way(way)
                break

    def relation(self, relation: osmium.osm.Relation) -> None:
        """Process a relation."""
        for member in relation.members:
            if member.type == osmium.osm.Node:
                if self._clip_polygon_contains(member.location):
                    self.writer.add_relation(relation)
                    break
