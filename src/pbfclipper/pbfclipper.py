#!/usr/bin/env python


"""Identify the smallest extract that covers the requested polygon."""


import datetime
import os
import tempfile

from .cache import Cache, NotInCache
from .cachingrequestssession import CachingRequestsSession
from .extractfinder import ExtractFinder
from .pbffileclipper import PbfFileClipper


import warnings
# warnings.filterwarnings(
#     "ignore",
#     message="Geometry is in a geographic CRS. Results from 'area' are likely incorrect. Use 'GeoSeries.to_crs()' to re-project geometries to a projected CRS before this operation."
# )
warnings.simplefilter("ignore")


class PbfClipper:
    """Retrieve an OpenStreetmap PBF extract clipped to a polygon."""

    def __init__(
            self,
            expire_after=datetime.timedelta(days=7),
            clean_cache=True
    ):
        """
        Initialise a PbdClipper.

        Arguments:
            expire_after (datetime.timedelta): refetch and recompute previously
                clipped extracts that are older than expire_after
            clean_cache (bool): clean expired extracts from cache upon init
        """
        self.cache = Cache(expire_after, clean_cache)

    def clip(self, polygon, output_filename):
        """
        Retrieve an .osm.pbf extract, clip it to polygon, and save it to output_filename.

        Arguments:
            polygon (shapely.geometry.Polygon):
                clip data to this polygon
            output_filename (str): output filename (*.osm.pbf)
        """
        try:
            cached_clipped_extract = self.cache.cached(str(polygon))
            with open(output_filename, "wb") as output_file:
                output_file.write(cached_clipped_extract)

        except NotInCache:
            # pyosmium is guessing from file extensions
            # let’s make sure we output to .osm.pbf,
            # then rename back if needed
            real_output_filename = output_filename
            if output_filename[-8:] != ".osm.pbf":
                output_filename += ".osm.pbf"

            extract_url = ExtractFinder().url_of_extract_that_covers(polygon)

            input_filename = tempfile.mkstemp(suffix=".osm.pbf")[1]
            with open(
                input_filename, "wb"
            ) as input_file, CachingRequestsSession() as session, session.get(
                extract_url
            ) as response:
                input_file.write(response.content)

            PbfFileClipper().clip(input_filename, polygon, output_filename)

            with open(output_filename, "rb") as output:
                self.cache.write_to_cache(str(polygon), output.read())

            os.unlink(input_filename)
            os.rename(output_filename, real_output_filename)
