#!/usr/bin/env python3

"""Clip an OpenStreetmap Protobuf file to the extents of a polygon."""


import argparse
import os
import os.path
import sys

import shapely.wkt

from .pbfclipper import PbfClipper


def main():
    """Clip an OpenStreetmap Protobuf file to the extents of a polygon."""
    argparser = argparse.ArgumentParser(
        description="Get an OpenStreetmap Protobuf file clipped to the "
        + "extents of a polygon."
    )
    argparser.add_argument(
        "--overwrite",
        action="store_true",
        help="Overwrite the output file if it exists"
    )
    argparser.add_argument(
        "-p", "--polygon",
        required=True,
        help="Get all data inside of this WKT polygon."
    )
    argparser.add_argument(
        "-o", "--output",
        required=True,
        help="Output file name (.osm.pbf)"
    )
    args = argparser.parse_args()

    polygon = shapely.wkt.loads(args.polygon)

    if os.path.exists(args.output):
        if args.overwrite:
            os.unlink(args.output)
        else:
            print(
                (
                    "Output file {:s} exists, "
                    + "use --overwrite to overwrite."
                ).format(args.output),
                file=sys.stderr
            )
            exit(1)

    PbfClipper().clip(
        polygon=polygon,
        output_filename=args.output
    )


if __name__ == "__main__":
    main()
