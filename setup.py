#!/usr/bin/env python

"""Compile the contained Cython module(s)."""

from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules=cythonize(["**/*.pyx"])
)
